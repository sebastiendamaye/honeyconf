-- honeypot1-emea-fr-grenoble_amun
-- Plugin id:9020

DELETE FROM plugin WHERE id = "9020";
DELETE FROM plugin_sid where plugin_id = "9020";

INSERT IGNORE INTO plugin (id, type, name, description) VALUES (9020, 1, 'honeypot1-emea-fr-grenoble_amun', 'Honeypot Grenoble Amun');
INSERT IGNORE INTO plugin_sid (plugin_id, sid, class_id, reliability, priority, name, subcategory_id, category_id) VALUES (9020, 1, NULL, 3, 1, 'honeypot1-emea-fr-grenoble_amun: generic', 225, 19);
