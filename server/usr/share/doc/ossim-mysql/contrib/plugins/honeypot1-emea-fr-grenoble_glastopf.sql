-- honeypot1-emea-fr-grenoble_glastopf
-- Plugin id:9030

DELETE FROM plugin WHERE id = "9030";
DELETE FROM plugin_sid where plugin_id = "9030";
INSERT IGNORE INTO plugin (id, type, name, description) VALUES (9030, 1, 'honeypot1-emea-fr-grenoble_glastopf', 'Honeypot Grenoble Glastopf');

INSERT IGNORE INTO plugin_sid (plugin_id, sid, class_id, reliability, priority, name, subcategory_id, category_id) VALUES (9030, 1, NULL, 3, 1, 'honeypot1-emea-fr-grenoble_glastopf: comments', 225, 19);
INSERT IGNORE INTO plugin_sid (plugin_id, sid, class_id, reliability, priority, name, subcategory_id, category_id) VALUES (9030, 2, NULL, 3, 1, 'honeypot1-emea-fr-grenoble_glastopf: dummy', 225, 19);
INSERT IGNORE INTO plugin_sid (plugin_id, sid, class_id, reliability, priority, name, subcategory_id, category_id) VALUES (9030, 3, NULL, 3, 1, 'honeypot1-emea-fr-grenoble_glastopf: favicon_ico', 225, 19);
INSERT IGNORE INTO plugin_sid (plugin_id, sid, class_id, reliability, priority, name, subcategory_id, category_id) VALUES (9030, 4, NULL, 3, 1, 'honeypot1-emea-fr-grenoble_glastopf: file_server', 225, 19);
INSERT IGNORE INTO plugin_sid (plugin_id, sid, class_id, reliability, priority, name, subcategory_id, category_id) VALUES (9030, 5, NULL, 3, 1, 'honeypot1-emea-fr-grenoble_glastopf: head', 225, 19);
INSERT IGNORE INTO plugin_sid (plugin_id, sid, class_id, reliability, priority, name, subcategory_id, category_id) VALUES (9030, 6, NULL, 3, 1, 'honeypot1-emea-fr-grenoble_glastopf: lfi', 225, 19);
INSERT IGNORE INTO plugin_sid (plugin_id, sid, class_id, reliability, priority, name, subcategory_id, category_id) VALUES (9030, 7, NULL, 3, 1, 'honeypot1-emea-fr-grenoble_glastopf: login', 225, 19);
INSERT IGNORE INTO plugin_sid (plugin_id, sid, class_id, reliability, priority, name, subcategory_id, category_id) VALUES (9030, 8, NULL, 3, 1, 'honeypot1-emea-fr-grenoble_glastopf: options', 225, 19);
INSERT IGNORE INTO plugin_sid (plugin_id, sid, class_id, reliability, priority, name, subcategory_id, category_id) VALUES (9030, 9, NULL, 3, 1, 'honeypot1-emea-fr-grenoble_glastopf: php_cgi_rce', 225, 19);
INSERT IGNORE INTO plugin_sid (plugin_id, sid, class_id, reliability, priority, name, subcategory_id, category_id) VALUES (9030, 10, NULL, 3, 1, 'honeypot1-emea-fr-grenoble_glastopf: phpinfo', 225, 19);
INSERT IGNORE INTO plugin_sid (plugin_id, sid, class_id, reliability, priority, name, subcategory_id, category_id) VALUES (9030, 11, NULL, 3, 1, 'honeypot1-emea-fr-grenoble_glastopf: phpmyadmin', 225, 19);
INSERT IGNORE INTO plugin_sid (plugin_id, sid, class_id, reliability, priority, name, subcategory_id, category_id) VALUES (9030, 12, NULL, 3, 1, 'honeypot1-emea-fr-grenoble_glastopf: rfi', 225, 19);
INSERT IGNORE INTO plugin_sid (plugin_id, sid, class_id, reliability, priority, name, subcategory_id, category_id) VALUES (9030, 13, NULL, 3, 1, 'honeypot1-emea-fr-grenoble_glastopf: robots', 225, 19);
INSERT IGNORE INTO plugin_sid (plugin_id, sid, class_id, reliability, priority, name, subcategory_id, category_id) VALUES (9030, 14, NULL, 3, 1, 'honeypot1-emea-fr-grenoble_glastopf: sqli', 225, 19);
INSERT IGNORE INTO plugin_sid (plugin_id, sid, class_id, reliability, priority, name, subcategory_id, category_id) VALUES (9030, 15, NULL, 3, 1, 'honeypot1-emea-fr-grenoble_glastopf: style_css', 225, 19);
INSERT IGNORE INTO plugin_sid (plugin_id, sid, class_id, reliability, priority, name, subcategory_id, category_id) VALUES (9030, 16, NULL, 3, 1, 'honeypot1-emea-fr-grenoble_glastopf: tomcat_manager', 225, 19);
INSERT IGNORE INTO plugin_sid (plugin_id, sid, class_id, reliability, priority, name, subcategory_id, category_id) VALUES (9030, 17, NULL, 3, 1, 'honeypot1-emea-fr-grenoble_glastopf: tomcat_status', 225, 19);
INSERT IGNORE INTO plugin_sid (plugin_id, sid, class_id, reliability, priority, name, subcategory_id, category_id) VALUES (9030, 18, NULL, 3, 1, 'honeypot1-emea-fr-grenoble_glastopf: trace', 225, 19);
INSERT IGNORE INTO plugin_sid (plugin_id, sid, class_id, reliability, priority, name, subcategory_id, category_id) VALUES (9030, 19, NULL, 3, 1, 'honeypot1-emea-fr-grenoble_glastopf: unknown', 225, 19);
