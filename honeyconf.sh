#!/bin/bash

###
# Author: Sebastien Damaye
# Revision: 2014.09.23.001
# Tested on Debian 7.6 64bit
#
# Features:
# - (un)installation of main honeypot components
# - manage (start and stop) services
# - supported packages: amun, glastopf
#


###################################################################
# CONFIGURATION
###################################################################
MYSQL_PASSWORD_ROOT="oopsoops"
MYSQL_PASSWORD_AMUN="Amun_P455w0rD"
###################################################################


###################################################################
# DO NOT TOUCH WHAT IS BELOW THIS LINE (unless you know what you're doing :)
#

###
# MISC FUNCTIONS
#
function quit {
    exit
}

function check_sudo {
    if [ "$(/usr/bin/whoami)" != "root" ]; then
        echo "***ERROR: Please start the program as priviledged user."
        quit
    fi
}

###
# INSTALL / UNINSTALL PACKAGES
#
function install_amun {
    # check if amun is already installed
    if [ -d /opt/amun/ ]; then
        /usr/bin/whiptail --title "Installation aborted" \
         --msgbox "Amun is already installed in /opt/amun. To force a reinstallation, remove this directory." 10 40
        main_menu
    fi
    
    if (/usr/bin/whiptail --title "Confirm installation of amun" \
     --yesno "Amun is a low interaction honeypot. Do you confirm the installation?" 8 78); then
        echo "Installing prerequisites ... "
        /usr/bin/aptitude update
        /usr/bin/debconf-apt-progress -- /usr/bin/aptitude -y install python2.7 python-psyco python-mysqldb \
         python-psycopg2 mysql-server wget
    
        echo -n "Downloading amun..."
        cd /tmp/
        /usr/bin/wget -q http://downloads.sourceforge.net/project/amunhoney/amun/amun-v0.1.9/amun-v0.1.9.tar.gz
        echo "[ OK ]"

        echo -n "Installing amun in /opt..."
        tar xzf amun-v0.1.9.tar.gz -C /opt/
        echo "[ OK ]"

        echo -n "Configuring amun..."
        ### Log events in syslog
        /bin/sed -i "s/^#[ \t]log\-syslog$/        log\-syslog/" /opt/amun/conf/amun.conf

        ### Database support
#        # create database
#        /usr/bin/mysql -u root --password=$MYSQL_PASSWORD_ROOT < amun_db.sql
#        # create specific user for amun and grant privs
#        /usr/bin/mysql -u root --password=$MYSQL_PASSWORD_ROOT -e "create user amun@localhost identified by '$MYSQL_PASSWORD_AMUN';"
#        /usr/bin/mysql -u root --password=$MYSQL_PASSWORD_ROOT -e "grant all privileges on amun_db.* to amun@localhost;"
#        # update conf
#        echo "[Log-MySQL]" > /opt/amun/conf/log-mysql.conf
#        echo "MySQLHost: 127.0.0.1" >> /opt/amun/conf/log-mysql.conf
#        echo "MySQLUser: amun" >> /opt/amun/conf/log-mysql.conf
#        echo "MySQLPass: $MYSQL_PASSWORD_AMUN" >> /opt/amun/conf/log-mysql.conf
#        echo "MySQLDB: amun_db" >> /opt/amun/conf/log-mysql.conf

        echo "[ OK ]"

        /usr/bin/whiptail --title "Installation finished" \
         --msgbox "Amun has been successfully installed.\nYou can now start it.\n\nPress OK to continue" 10 40
    fi
    main_menu
}

function uninstall_amun {
    if (/usr/bin/whiptail --title "Confirm uninstallation of amun" \
     --yesno "This will uninstall amun. Do you want to continue?" 8 78); then
        echo -n "Uninstalling amun..."
        # search PID and kill it
        kill `ps aux | grep amun_server | grep python | awk '{print $2}'`
        # Drop user and database
    #    /usr/bin/mysql -u root --password=$MYSQL_PASSWORD_ROOT -e "drop user amun@localhost;"
    #    /usr/bin/mysql -u root --password=$MYSQL_PASSWORD_ROOT -e "drop database amun_db;"
        # Remove folder
        rm -fR /opt/amun/
        echo "[ OK ]"
        /usr/bin/whiptail --title "Amun uninstalled" --msgbox "Amun has been successfully uninstalled.\n\nPress OK to continue" 10 40
    fi
    main_menu
}

function install_glastopf {
    # check if glastopf is already installed
    if [ -d /opt/myhoneypot/ ]; then
        /usr/bin/whiptail --title "Installation aborted" \
         --msgbox "Glastopf is already installed in /opt/myhoneypot/. To force a reinstallation, remove this directory." 10 40
        main_menu
    fi

    if (/usr/bin/whiptail --title "Confirm installation of amun" \
     --yesno "Glastopf is a low interaction honeypot. Do you confirm the installation?" 8 78); then
        echo "Installing prerequisites ... "

        # add backports repo
        echo "deb http://ftp.debian.org/debian/ wheezy-backports main" >> /etc/apt/sources.list
        
        /usr/bin/aptitude update
        /usr/bin/debconf-apt-progress -- /usr/bin/aptitude -y install python python-openssl python-gevent \
         libevent-dev python-dev build-essential make \
         python-argparse python-chardet python-requests python-sqlalchemy python-lxml \
         python-beautifulsoup mongodb python-pip python-dev python-setuptools \
         g++ git php5 php5-dev php5-cgi liblapack-dev gfortran \
         libxml2-dev libxslt1-dev \
         libmysqlclient-dev
        echo "Installation of python dependencies..."
        pip install --upgrade distribute
        pip install --upgrade greenlet

        echo "Installation of PHP sandbox..."
        # Install and configure the PHP sandbox
        cd /opt
        git clone git://github.com/glastopf/BFR.git
        cd BFR/
        phpize
        ./configure --enable-bfr
        make
        make install

        #Copy the search path to bfr.so and add it to php.ini. It can look like this:
        #zend_extension = /usr/lib/php5/20100525/bfr.so

        echo "Installation of glastopf..."
        #install glastopf
        pip install glastopf

        echo "Configuration of glastopf..."
        # configuration
        mkdir -p /opt/myhoneypot/
        cd /opt/myhoneypot/
        # start glastopf to generate a default conf file
        /usr/sbin/service apache2 stop
        cd /opt/myhoneypot/
        glastopf-runner &
        #wait 7 seconds to ensure service is really started
        sleep 7

        # stop glastopf
        kill `ps aux | grep glastopf | grep python | awk '{print $2}'`
        # wait 2 seconds to ensure service is really stopped
        sleep 2

        # patch conf file to activate syslog logging
        wget https://dl.dropboxusercontent.com/u/10761700/patch_glastopf_conf_syslog.txt
        patch -p0 < patch_glastopf_conf_syslog.txt

        /usr/bin/whiptail --title "Installation finished" \
         --msgbox "Glastopf has been successfully installed.\nYou can now start it.\n\nPress OK to continue" 10 40
    fi
    main_menu
}

function uninstall_glastopf {
    if (/usr/bin/whiptail --title "Confirm uninstallation of glastopf" \
     --yesno "This will uninstall glastopf. Do you want to continue?" 8 78); then
        echo "*** TO BE COMPLETED ***"
    fi

    # search PID and kill it
    kill `ps aux | grep glastopf | grep python | awk '{print $2}'`
    
    # Uinstall glastopf
    ###
    ###
    ###
    ###
    ### TO BE COMPLETED
    ###
    ###
    ###
    ###

    main_menu
}

###
# START/STOP DAEMONS
#
function start_amun {
    echo "Starting amun..."
    cd /opt/amun/
    /usr/bin/python amun_server.py &
    /usr/bin/whiptail --title "Amun started" --msgbox "Amun started." 10 40
    main_menu
}

function stop_amun {
    echo "Stopping amun ... "
    # search PID and kill it
    kill `ps aux | grep amun_server | grep python | awk '{print $2}'`
    /usr/bin/whiptail --title "Amun stopped" --msgbox "Amun stopped." 10 40
    main_menu
}

function start_glastopf {
    # free port 80
    /usr/sbin/service apache2 stop
    # start glastopf
    cd /opt/myhoneypot/
    glastopf-runner &
	/usr/bin/clear
    /usr/bin/whiptail --title "Glastopf started" --msgbox "Glastopf started." 10 40
    main_menu
}

function stop_glastopf {
    echo "Stopping glastopf ... "
    # search PID and kill it
    kill `ps aux | grep glastopf | grep python | awk '{print $2}'`
    /usr/bin/whiptail --title "Glastopf stopped" --msgbox "Glastopf stopped." 10 40
    main_menu
}


###
# MENUS
#
function manage_packages_menu {
    /usr/bin/whiptail --title "Options" --menu "Choose an option" 20 60 10 \
                   "Install amun" "(Install amun package)" \
                   "Uninstall amun" "(uninstall amun package)"  \
                   "Install glastopf" "(Install glastopf package)" \
                   "Uninstall glastopf" "(uninstall glastopf package)"  \
                   "Back" "(Go back to main menu)" 2> choice

    case $(cat choice) in
        "Install amun")
            install_amun
            ;;
        "Uninstall amun")
            uninstall_amun
            ;;
        "Install glastopf")
            install_glastopf
            ;;
        "Uninstall glastopf")
            uninstall_glastopf
            ;;
        "Back")
            main_menu
            ;;
    esac
}

function manage_daemons_menu {
    /usr/bin/whiptail --title "Options" --menu "Choose an option" 20 60 10 \
                   "Start amun" "(Start amun daemon)" \
                   "Stop amun" "(Stop amun daemon)"  \
                   "Start glastopf" "(Start glastopf daemon)" \
                   "Stop glastopf" "(Stop glastopf daemon)"  \
                   "Back" "(Go back to main menu)" 2> choice

    case $(cat choice) in
        "Start amun")
            start_amun
            ;;
        "Stop amun")
            stop_amun
            ;;
        "Start glastopf")
            start_glastopf
            ;;
        "Stop glastopf")
            stop_glastopf
            ;;
        "Back")
            main_menu
            ;;
    esac
}

function main_menu {
	/usr/bin/clear
    /usr/bin/whiptail --title "Options" --menu "Choose an option" 15 55 4 \
                   "Manage packages" "(Install and remove packages)" \
                   "Manage daemons" "(Start and stop daemons)"  \
                   "Quit" "(Exit the application)" 2> choice

    case $(cat choice) in
        "Manage packages")
            manage_packages_menu
            ;;
        "Manage daemons")
            manage_daemons_menu
            ;;
        "Quit")
            quit
            ;;
    esac
}


### Main program
check_sudo
main_menu
