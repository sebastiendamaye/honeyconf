#!/usr/bin/env python


print """
-- honeypot1-emea-fr-grenoble_glastopf
-- Plugin id:9030

DELETE FROM plugin WHERE id = "9030";
DELETE FROM plugin_sid where plugin_id = "9030";
INSERT IGNORE INTO plugin (id, type, name, description) VALUES (9030, 1, 'honeypot1-emea-fr-grenoble_glastopf', 'Honeypot Grenoble Glastopf');
"""

a = ['comments', 'dummy', 'favicon_ico', 'file_server', 'head', 'lfi', 'login', 'options', 'php_cgi_rce', 'phpinfo', 'phpmyadmin', 'rfi', 'robots', 'sqli', 'style_css', 'tomcat_manager', 'tomcat_status', 'trace', 'unknown']

n = 1
for i in a:

    print """INSERT IGNORE INTO plugin_sid (plugin_id, sid, class_id, reliability, priority, name, subcategory_id, category_id) VALUES (9030, %s, NULL, 3, 1, 'honeypot1-emea-fr-grenoble_glastopf: %s', 225, 19);""" % (n, i)

    n+=1