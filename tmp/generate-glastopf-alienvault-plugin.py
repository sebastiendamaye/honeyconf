#!/usr/bin/env python

a = ['comments', 'dummy', 'favicon_ico', 'file_server', 'head', 'lfi', 'login', 'options', 'php_cgi_rce', 'phpinfo', 'phpmyadmin', 'rfi', 'robots', 'sqli', 'style_css', 'tomcat_manager', 'tomcat_status', 'trace', 'unknown']

n = 1
for i in a:
    print ""
    print """[honeypot1-emea-fr-grenoble_glastopf_%s]""" % i
    print """event_type=event"""
    print """regexp="(?P<date>\w{3}\s+\d{1,2}\s\d\d:\d\d:\d\d)\s+(?P<sensor>\S+)\s+Glaspot:\s%s\sattack\smethod\sfrom\s(?P<src>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}):(?P<srcport>\d+)\sagainst\s[a-zA-Z\S]+:(?P<dstport>\d+)\.\s\[(?P<method>(GET|POST))\s(?P<url>.+)\]" """ % i
    print """date={normalize_date($date)}"""
    print """sensor={$sensor}"""
    print """plugin_sid=%s""" % n
    print """src_ip={$src}"""
    print """src_port={$srcport}"""
    print """dst_port={$dstport}"""
    print """userdata1={$method}"""
    print """userdata2={$url}"""
    n+=1